import { h } from "preact";
import style from "./style.css";
import { checkPassword } from "@esri-devsummit-2022-tools/my-insecure-password-checker";
import { useState } from "preact/hooks";

const Home = () => {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [loginError, setLoginError] = useState(false);

    const onUsernameInput = (e) => {
        const { value } = e.target;
        setUsername(value);
    };

    const onPasswordInput = (e) => {
        const { value } = e.target;
        setPassword(value);
    };

    const onSubmit = (e) => {
        if (checkPassword(username, password)) {
            console.log("logged in");
            setLoginError(false);
            window.location.href = `/profile/${username}`;
        } else {
            console.error("Error logging in");
            setLoginError(true);
        }
    };

    return (
        <div class={style.home}>
            <h1>Login</h1>
            <div>
                <div class={style.row}>
                    <div class={style.label}>
                        <label>Username</label>
                    </div>
                    <input
                        onInput={onUsernameInput}
                        value={username}
                        type="text"
                    ></input>
                </div>
                <div class={style.row}>
                    <div class={style.label}>
                        <label>Password</label>
                    </div>
                    <input
                        onInput={onPasswordInput}
                        value={password}
                        type="password"
                    ></input>
                </div>
                <div class={style.row}>
                    <button onClick={onSubmit}>Submit</button>
                </div>
                <div
                    class={style.row}
                    style={{
                        visibility: loginError ? "visible" : "hidden",
                        color: "red",
                    }}
                >
                    Error Logging In
                </div>
            </div>
        </div>
    );
};

export default Home;
